PROJ_NAME=main

CC=arm-none-eabi-gcc
AR=arm-none-eabi-ar
OBJCOPY=arm-none-eabi-objcopy
OBJDUMP=arm-none-eabi-objdump
SIZE=arm-none-eabi-size

CFLAGS += -DSTM32F429xx -g
CFLAGS += -mfloat-abi=soft
CFLAGS += -Wall -std=c99 -O0 -fdata-sections -ffunction-sections
CFLAGS += -mlittle-endian -mcpu=cortex-m4 -mthumb -mthumb-interwork

CFLAGS+=-IDrivers/CMSIS/Include
CFLAGS+=-IDrivers/CMSIS/Device/ST/STM32F4xx/Include
CFLAGS+=-IDrivers/STM32F4xx_HAL_Driver/Inc
CFLAGS+=-IDrivers/BSP/STM32F429I-Discovery
CFLAGS+=-IDrivers/BSP/Components/**/
CFLAGS+=-IInc
CFLAGS += -includeInc/stm32f4xx_hal_conf.h

LDFLAGS += -L. -Wl,--gc-sections -specs=nano.specs
LDLIBS += -lhal -lbsp

LDSCRIPT_INC="./SW4STM32/stm32f429-template Configuration"

HALSRC = $(wildcard Drivers/STM32F4xx_HAL_Driver/Src/*.c)
HALOBJ = $(HALSRC:.c=.o)

BSPSRC = $(wildcard Drivers/BSP/STM32F429I-Discovery/*.c)
BSPSRC+= $(wildcard Drivers/BSP/Components/**/*.c)
BSPOBJ = $(BSPSRC:.c=.o)

SRCS += ./Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc/startup_stm32f429xx.s
SRCS += ./Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/system_stm32f4xx.c
SRCS += $(wildcard Src/*.c)

$(PROJ_NAME).elf: libhal.a libbsp.a $(SRCS)
	$(CC) --specs=nosys.specs $(CFLAGS) $(SRCS) $(LDFLAGS) $(LDLIBS) -o $@ -L$(LDSCRIPT_INC) -TSTM32F429ZITx_FLASH.ld
	$(OBJCOPY) -O ihex $(PROJ_NAME).elf $(PROJ_NAME).hex
	$(OBJCOPY) -O binary $(PROJ_NAME).elf $(PROJ_NAME).bin
	$(OBJDUMP) -St $(PROJ_NAME).elf >$(PROJ_NAME).lst
	$(SIZE) $(PROJ_NAME).elf


libhal.a: $(HALOBJ)
	$(AR) -r $@ $(HALOBJ)

libbsp.a: $(BSPOBJ)
	$(AR) -r $@ $(BSPOBJ)

clean:
	rm -f $(HALOBJ) $(BSPOBJ) libhal.a libbsp.a $(PROJ_NAME).elf $(PROJ_NAME).bin $(PROJ_NAME).hex $(PROJ_NAME).lst
