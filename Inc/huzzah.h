#ifndef HUZZAH_H
#define HUZZAH_H

#include <stdint.h>

#define HUZZAH_RESET_PORT GPIOA
#define HUZZAH_RESET_PIN GPIO_PIN_1

void huzzah_send_command(char *command, int resc, char *resv);
int huzzah_wifi_status();
uint8_t huzzah_await_connection();
void huzzah_connect_station(char *ssid, char *password);
void huzzah_http_get(char *host, char *url, int port, int resc, char *resv);

#endif
