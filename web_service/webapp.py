# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import os
from flask import Flask, request, redirect, url_for, render_template
from werkzeug import secure_filename
# import qrtools
# from pattern import patternRecognition

UPLOAD_FOLDER = 'uploads/'
ALLOWED_EXTENSIONS = set(['txt','pdf','jpg','jpeg','png','svg'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/index")
def index():
    return render_template('index.html')

@app.route("/test")
def test():
    return render_template('index.html')

@app.route("/uploads/test.txt")
def put():
    print vars(request)
    return 'hello'

@app.route("/", methods=['GET', 'POST'])
def pattern():
    if request.method == 'POST':
        # print request.stream.read()
        print request.data
        return 'hello'
        # file = request.files['file']
        # if file and allowed_file(file.filename):
        #     filename = secure_filename(file.filename)
        #     file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        #     txtFile = open(os.path.join(app.config['UPLOAD_FOLDER'], filename), 'r')
        #     print 'Uploaded content =',txtFile.read()
            # result, num = patternRecognition(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # print result, num
            # return "Upload test"
            # return qr.data
            # return redirect(url_for('pattern'))
    return """
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    <p>%s</p>
    """ % "<br>".join(os.listdir(app.config['UPLOAD_FOLDER'],))

@app.route("/upload", methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            qr = qrtools.QR()
            qr.decode(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            print qr.data
#            return qr.data
            return redirect(url_for('upload'))
    return """
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    <p>%s</p>
    """ % "<br>".join(os.listdir(app.config['UPLOAD_FOLDER'],))

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)
