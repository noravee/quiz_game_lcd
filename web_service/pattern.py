# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 15:24:21 2016

@author: DELL
"""


import cv2
import numpy as np
from matplotlib import pyplot as plt

def patternRecognition(filename):
    try:
        dimension = 300             # Dimension of output image
        gridRatio = dimension/5     # Range of one grid(5x5)
        #shapeNumber = 25            # Number of all contour
        #borderNumber = 16           # Number of border contour
        #diff = 30
        cornerPoint = [[0,0],[0,0],[0,0],[0,0]]
        binaryCode = [0,0,0,0,0,0,0,0,0]
        decimalNumber = 0
        result = False

        ### Load image file ###
        #image = cv2.imread('dot_pattern4.jpg')
        image = cv2.imread(filename)

        rows,cols,ch = image.shape    # get rows and column pixel of image

        blur = cv2.blur(image, (3,3))
        #cv2.imshow("Frame", blur)

        ### Set upper and lower color(black) level ###
        blackLower = np.array([0,0,0],dtype="uint8")
        blackUpper = np.array([50,50,50], dtype="uint8")

        blackThresh = cv2.inRange(blur, blackLower, blackUpper)

        ### Find contours in the threshold image ###
        blackContours,blackHierarchy = cv2.findContours(blackThresh,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

        count = 0
        xMin = blackContours[0][0][0][0]
        xMax = 0
        yMin = blackContours[0][0][0][1]
        yMax = 0

        indexC = 0
        indexA = 0

        for cnt in blackContours:
            # Calculate the centroid poin of each contour
            M = cv2.moments(cnt)
            cx,cy = int(M['m10']/M['m00']), int(M['m01']/M['m00'])

            # Find approximate points of each contour
            # For rectangle and triangle contour, approximate points is corner points
            peri = cv2.arcLength(cnt, True)
            approx = cv2.approxPolyDP(cnt, 0.04 * peri, True)

            if len(approx) == 3:    # if contour is triangle
                cornerPoint[indexA] = [cx,cy]   # collect centroid of triangle
                indexA += 1

            elif len(approx) == 4:  # if contour is rectangle
                cornerPoint[indexA] = [cx,cy]   # collect centroid of rectangle
                indexA += 1


            for a in approx:
                # Plot the corner point with green point
                #cv2.circle(blur,(a[0][0],a[0][1]),1,(0,255,0),-1)
                ### Update upper and lower bound of x and y ###
                if a[0][0] > xMax:
                    xMax = a[0][0]
                elif a[0][0] < xMin:
                    xMin = a[0][0]

                if a[0][1] > yMax:
                    yMax = a[0][1]
                elif a[0][1] < yMin:
                    yMin = a[0][1]
            indexC += 1

        ## end for loop

        ### Match four of corner points to the nearest corner point of new perspective ###
        per1 = 0
        per2 = 0
        per3 = 0
        per4 = 0
        xDiff = xMax-xMin
        yDiff = yMax-yMin
        cornerPointNew = [[0,0],[0,0],[0,0],[0,0]]

        for c in cornerPoint:
            if (c[0] >= xMin) & (c[0] <= xMax-(xDiff/2)) & (c[1] >= yMin) & (c[1] <= yMax-(yDiff/2)) :
                cornerPointNew[0] = c
                per1= 1
                # print('1')
            elif (c[0] > xMax-(xDiff/2)) & (c[0] <= xMax) & (c[1] >= yMin) & (c[1] <= yMax-(yDiff/2)) :
                cornerPointNew[1] = c
                per2= 1
                # print('2')
            elif (c[0] > xMax-(xDiff/2)) & (c[0] <= xMax) & (c[1] > yMax-(yDiff/2)) & (c[1] <= yMax) :
                cornerPointNew[2] = c
                per4= 1
                # print('4')
            elif (c[0] >= xMin) & (c[0] <= xMax-(xDiff/2)) & (c[1] > yMax-(yDiff/2)) & (c[1] <= yMax) :
                cornerPointNew[3] = c
                per3= 1
                # print('3')

        ### Check that we can match four of new conner points ###
        if (per1+per2+per3+per4 == 4):

            ### Adjust image to suitable perspective in dimension that we want ###

            #pts1 = np.float32([[1,1],[152,1],[152,148],[1,148]])
            #pts1 = np.float32([[716,461],[1371,283],[1544,918],[888,1097]])
            pts1 = np.float32([cornerPointNew[0],cornerPointNew[1],cornerPointNew[2],cornerPointNew[3]])
            pts2 = np.float32([[0,0],[dimension,0],[dimension,dimension],[0,dimension]])

            M = cv2.getPerspectiveTransform(pts1,pts2)
            dst = cv2.warpPerspective(image,M,(dimension,dimension))

            # plt.subplot(121),plt.imshow(image),plt.title('Input')
            # plt.subplot(122),plt.imshow(dst),plt.title('Output')
            # plt.show()

            ### Find contour againto detect four of corner sign (3 rectangle and 1 triangle) ###
            blur2 = cv2.blur(dst, (3,3))

            blackThresh2 = cv2.inRange(blur2, blackLower, blackUpper)
            blackContours2,blackHierarchy2 = cv2.findContours(blackThresh2,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

            count = 0
            countOne = 0

            for cnt in blackContours2:
                M = cv2.moments(cnt)
                cx,cy = int(M['m10']/M['m00']), int(M['m01']/M['m00'])

                peri = cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, 0.04 * peri, True)

                ### Try to detect triangle contour and check the its position ###
                ### triangle coutour must be the bottom right corner of image ###
                ### we have to rotate image to adjust triangle to the bottom right corner of image ###
                if len(approx) == 3:
                    if (cx >= 0) & (cx <= dimension/2) & (cy >= 0) & (cy <= dimension/2) :
                        M = cv2.getRotationMatrix2D((dimension/2,dimension/2),180,1)
                        dst = cv2.warpAffine(dst,M,(dimension,dimension))
                        # print('11')
                    elif (cx > dimension/2) & (c[0] <= dimension) & (cy >= 0) & (cy <= dimension/2) :
                        M = cv2.getRotationMatrix2D((dimension/2,dimension/2),-90,1)
                        dst = cv2.warpAffine(dst,M,(dimension,dimension))
                        # print('12')
        #            elif (cx > dimension/2) & (cx <= dimension) & (cy > dimension/2) & (cy <= dimension) :
        #                cornerPointNew[2] = c
        #                per4= 1
        #                print('14')
                    elif (cx >= 0) & (cx <= dimension/2) & (cy > dimension/2) & (cy <= dimension) :
                        M = cv2.getRotationMatrix2D((dimension/2,dimension/2),90,1)
                        dst = cv2.warpAffine(dst,M,(dimension,dimension))
                        # print('13')

            # plt.subplot(121),plt.imshow(image),plt.title('Input')
            # plt.subplot(122),plt.imshow(dst),plt.title('Output')
            # plt.show()

            ### Find contour again to locate position of all contour ###
            blur2 = cv2.blur(dst, (3,3))

            blackThresh2 = cv2.inRange(blur2, blackLower, blackUpper)
            blackContours2,blackHierarchy2 = cv2.findContours(blackThresh2,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
            for cnt in blackContours2:

                # Calculate the centroid of each contour
                M = cv2.moments(cnt)
                cx,cy = int(M['m10']/M['m00']), int(M['m01']/M['m00'])
                # plot the centroid point with red color point
                #cv2.circle(blur2,(cx,cy),1,(0,0,255),-1)

                ### Generate binary code ###
                ### each 8 grid in the center (exceptthe bottom left corner grid) is refer to each binary bit (8 bit) ###
                ### bottom to top and rigth to left ###
                if (cx >= gridRatio) & (cx <= gridRatio*2) & (cy >= gridRatio) & (cy <= gridRatio*2):
                    binaryCode[0] = 1
                    decimalNumber += 128
                    countOne += 1
                elif (cx >= gridRatio*2) & (cx <= gridRatio*3) & (cy >= gridRatio) & (cy <= gridRatio*2):
                    binaryCode[1] = 1
                    decimalNumber += 64
                    countOne += 1
                elif (cx >= gridRatio*3) & (cx <= gridRatio*4) & (cy >= gridRatio) & (cy <= gridRatio*2):
                    binaryCode[2] = 1
                    decimalNumber += 32
                    countOne += 1
                elif (cx >= gridRatio) & (cx <= gridRatio*2) & (cy >= gridRatio*2) & (cy <= gridRatio*3):
                    binaryCode[3] = 1
                    decimalNumber += 16
                    countOne += 1
                elif (cx >= gridRatio*2) & (cx <= gridRatio*3) & (cy >= gridRatio*2) & (cy <= gridRatio*3):
                    binaryCode[4] = 1
                    decimalNumber += 8
                    countOne += 1
                elif (cx >= gridRatio*3) & (cx <= gridRatio*4) & (cy >= gridRatio*2) & (cy <= gridRatio*3):
                    binaryCode[5] = 1
                    decimalNumber += 4
                    countOne += 1
                elif (cx >= gridRatio) & (cx <= gridRatio*2) & (cy >= gridRatio*3) & (cy <= gridRatio*4):
                    binaryCode[6] = 1
                    decimalNumber += 2
                    countOne += 1
                elif (cx >= gridRatio*2) & (cx <= gridRatio*3) & (cy >= gridRatio*3) & (cy <= gridRatio*4):
                    binaryCode[7] = 1
                    decimalNumber += 1
                    countOne += 1
                count += 1
            ### Set checksum bit. ###
            ### If in eight bit of binary code have odd number of "1",checksum bit is "1". Otherwise is "0" ###
            if (countOne%2) != 0:
                binaryCode[8] = 1

            # print(binaryCode)
            result = True

            # show the frame
            #cv2.imshow("Frame", blur)
            #print(count)
        else:
            # print('Try Again !!')
            result = False
    except:
        result = False
        decimalNumber = 0

    if cv2.waitKey(0) & 0xff == 27:
        cv2.destroyAllWindows()

    return result,decimalNumber
### End of function ###

# call function
#result,decimalNumber = patternRecognition('dot_pattern4.jpg')
#print(result)
#print(decimalNumber)
