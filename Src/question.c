#ifndef NUMBER_OF_QUESTION
#define NUMBER_OF_QUESTION 50
#endif

#include "structure.h"

extern struct questions q[NUMBER_OF_QUESTION];

void generateQuestionDatabase(){
  //question 1
  //struct questions q[15];
  q[0].qid = 0;
  q[0].question = "who is the first person to step on the moon?\r\n";
  q[0].choice[0] = "Bret 'HITMAN' hart\r\n";
  q[0].choice[1] = "Alexander the Great\r\n";
  q[0].choice[2] = "Neil Armstrong\r\n";
  q[0].choice[3] = "Koffi Aanon\r\n";
  q[0].answer = 3;
  q[0].used = 0;
  q[0].used = 0;
  q[0].categories = "History";
  //question 2
  q[1].qid = 1;
  q[1].question = "what is the capital of holland?\r\n";
  q[1].choice[0] = "Luxemburg\r\n";
  q[1].choice[1] = "Amsterdam\r\n";
  q[1].choice[2] = "Munich\r\n";
  q[1].choice[3] = "Budapest\r\n";
  q[1].answer = 2;
  q[1].used = 0;
  q[1].categories = "General";
  //question 3
  q[2].qid = 2;
  q[2].question = "Which legal document states a person's wishes regarding the disposal of their property after death?\r\n";
  q[2].choice[0] = "wish\r\n";
  q[2].choice[1] = "will\r\n";
  q[2].choice[2] = "shall\r\n";
  q[2].choice[3] = "screw you\r\n";
  q[2].answer = 2;
  q[2].used = 0;
  q[2].categories = "General";
  //question 4
  q[3].qid = 3;
  q[3].question = "Which of these sports does not involve playing with the ball?\r\n";
  q[3].choice[0] = "Football\r\n";
  q[3].choice[1] = "VolleyBall\r\n";
  q[3].choice[2] = "Archery\r\n";
  q[3].choice[3] = "Getting laid\r\n";
  q[3].answer = 3;
  q[3].used = 0;
  q[3].categories = "General";
  //question 5
  q[4].qid = 4;
  q[4].question = "George Bush was the president of which country?\r\n";
  q[4].choice[0] = "Afghanistan\r\n";
  q[4].choice[1] = "Iraq\r\n";
  q[4].choice[2] = "Pakistan\r\n";
  q[4].choice[3] = "USA\r\n";
  q[4].answer = 4;
  q[4].used = 0;
  q[4].categories = "History";
  //question 6
  q[5].qid = 5;
  q[5].question = "Who won the footbal world cup in 2006?\r\n";
  q[5].choice[0] = "India\r\n";
  q[5].choice[1] = "Iran\r\n";
  q[5].choice[2] = "Iraq\r\n";
  q[5].choice[3] = "Italy\r\n";
  q[5].answer = 4;
  q[5].used = 0;
  q[5].categories = "History";
  //question 7
  q[6].qid = 6;
  q[6].question = "William Shakespeare was ...\r\n";
  q[6].choice[0] = "Cheer leader\r\n";
  q[6].choice[1] = "poet\r\n";
  q[6].choice[2] = "Singer\r\n";
  q[6].choice[3] = "was the reason i failed in english class\r\n";
  q[6].answer = 2;
  q[6].used = 0;
  q[6].categories = "History";
  //question 8
  q[7].qid = 7;
  q[7].question = "Who is the king of the jungle?\r\n";
  q[7].choice[0] = "Zebra\r\n";
  q[7].choice[1] = "Lion\r\n";
  q[7].choice[2] = "Spider\r\n";
  q[7].choice[3] = "Snake\r\n";
  q[7].answer = 2;
  q[7].used = 0;
  q[7].categories = "General";
  //question 9
  q[8].qid = 8;
  q[8].question = "what is the capital of New Zealand\r\n";
  q[8].choice[0] = "Welligton\r\n";
  q[8].choice[1] = "Washington\r\n";
  q[8].choice[2] = "Auckland\r\n";
  q[8].choice[3] = "Victoria\r\n";
  q[8].answer = 1;
  q[8].used = 0;
  q[8].categories = "General";
  //question 10
  q[9].qid = 9;
  q[9].question = "What is the company Super Mario official Masscot of?\r\n";
  q[9].choice[0] = "sega\r\n";
  q[9].choice[1] = "Microsoft\r\n";
  q[9].choice[2] = "sony\r\n";
  q[9].choice[3] = "nintendo\r\n";
  q[9].answer = 4;
  q[9].used = 0;
  q[9].categories = "History";
  //question 11
  q[10].qid = 10;
  q[10].question = "In the cartoon show Tom&Jerry =  Tom is a ...\r\n";
  q[10].choice[0] = "Dog\r\n";
  q[10].choice[1] = "Cat\r\n";
  q[10].choice[2] = "Human\r\n";
  q[10].choice[3] = "Elepant\r\n";
  q[10].answer = 2;
  q[10].used = 0;
  q[10].categories = "General";
  //question 12
  q[11].qid = 11;
  q[11].question = "How many faces does a tradional Die has\r\n";
  q[11].choice[0] = "Two\r\n";
  q[11].choice[1] = "Six\r\n";
  q[11].choice[2] = "Twelve\r\n";
  q[11].choice[3] = "Four\r\n";
  q[11].answer = 2;
  q[11].used = 0;
  q[11].categories = "General";
  //question 13
  q[12].qid = 12;
  q[12].question = "According to common saying what will set you free ?\r\n";
  q[12].choice[0] = "Guilt\r\n";
  q[12].choice[1] = "Ego\r\n";
  q[12].choice[2] = "Truth\r\n";
  q[12].choice[3] = "........\r\n";
  q[12].answer = 3;
  q[12].used = 0;
  q[12].categories = "General";
  //question 14
  q[13].qid = 13;
  q[13].question = "What is name of the main character in the game GTA VICE CITY?\r\n";
  q[13].choice[0] = "Nico\r\n";
  q[13].choice[1] = "Tommy Vercetti\r\n";
  q[13].choice[2] = "Roman\r\n";
  q[13].choice[3] = "CJ\r\n";
  q[13].answer = 2;
  q[13].used = 0;
  q[13].categories = "History";
  //question 15
  q[14].qid = 14;
  q[14].question = "How many zeros are there in a million\r\n";
  q[14].choice[0] = "Five\r\n";
  q[14].choice[1] = "Six\r\n";
  q[14].choice[2] = "Seven\r\n";
  q[14].choice[3] = "Eight\r\n";
  q[14].answer = 2;
  q[14].used = 0;
  q[14].categories = "History";
  //question 16
  q[15].qid = 15;
  q[15].question = "Which people are associated with eye patches?\r\n";
  q[15].choice[0] = "Gladiators\r\n";
  q[15].choice[1] = "Knights\r\n";
  q[15].choice[2] = "Kings\r\n";
  q[15].choice[3] = "Pirates\r\n";
  q[15].answer = 4;
  q[15].used = 0;
  q[15].categories = "General";
  //question 17
  q[16].qid = 16;
  q[16].question = "All matter is made from tiny particles called...\r\n";
  q[16].choice[0] = "Bricks\r\n";
  q[16].choice[1] = "Sand\r\n";
  q[16].choice[2] = "Atoms\r\n";
  q[16].choice[3] = "Salt\r\n";
  q[16].answer = 3;
  q[16].used = 0;
  q[16].categories = "General";
  //question 18
  q[17].qid = 17;
  q[17].question = "Yen is the currency of which country?\r\n";
  q[17].choice[0] = "Japan\r\n";
  q[17].choice[1] = "Mexico\r\n";
  q[17].choice[2] = "India\r\n";
  q[17].choice[3] = "Russia\r\n";
  q[17].answer = 1;
  q[17].used = 0;
  q[17].categories = "General";
  //question 19
  q[18].qid = 18;
  q[18].question = "Which fast food place is known for its trademark Golden Arches?\r\n";
  q[18].choice[0] = "Burger King\r\n";
  q[18].choice[1] = "Taco Bell\r\n";
  q[18].choice[2] = "Arby's\r\n";
  q[18].choice[3] = "McDonalds\r\n";
  q[18].answer = 4;
  q[18].used = 0;
  q[18].categories = "General";
  //question 20
  q[19].qid = 19;
  q[19].question = "Carpenter, army, and fire are what types of insect?\r\n";
  q[19].choice[0] = "Centipedes\r\n";
  q[19].choice[1] = "Ants\r\n";
  q[19].choice[2] = "Moths\r\n";
  q[19].choice[3] = "Grasshoppers\r\n";
  q[19].answer = 2;
  q[19].used = 0;
  q[19].categories = "General";
  //question 21
  q[20].qid = 20;
  q[20].question = "Who was tied to a rock and tortured by a giant vulture?\r\n";
  q[20].choice[0] = "Hercules\r\n";
  q[20].choice[1] = "Atlas\r\n";
  q[20].choice[2] = "Prometheus\r\n";
  q[20].choice[3] = "Perseus\r\n";
  q[20].answer = 3;
  q[20].used = 0;
  q[20].categories = "History";
  //question 22
  q[21].qid = 21;
  q[21].question = "Which of these African countries shares a border with Togo?\r\n";
  q[21].choice[0] = "Ethiopia\r\n";
  q[21].choice[1] = "Ghana\r\n";
  q[21].choice[2] = "Mozambique\r\n";
  q[21].choice[3] = "Botswana\r\n";
  q[21].answer = 2;
  q[21].used = 0;
  q[21].categories = "General";
  //question 23
  q[22].qid = 22;
  q[22].question = "Which creature hangs upside down from the branches of trees?\r\n";
  q[22].choice[0] = "Sloth\r\n";
  q[22].choice[1] = "Badger\r\n";
  q[22].choice[2] = "Armadillo\r\n";
  q[22].choice[3] = "Porcupine\r\n";
  q[22].answer = 1;
  q[22].used = 0;
  q[22].categories = "General";
  //question 24
  q[23].qid = 23;
  q[23].question = "Which of these countries is closest to the Cayman Islands?\r\n";
  q[23].choice[0] = "Japan\r\n";
  q[23].choice[1] = "Iceland\r\n";
  q[23].choice[2] = "Jamaica\r\n";
  q[23].choice[3] = "Madagascar\r\n";
  q[23].answer = 4;
  q[23].used = 0;
  q[23].categories = "General";
  //question 25
  q[24].qid = 24;
  q[24].question = "Which of these is the name of a bird?\r\n";
  q[24].choice[0] = "Rapid\r\n";
  q[24].choice[1] = "Swift\r\n";
  q[24].choice[2] = "Quick\r\n";
  q[24].choice[3] = "Fleet\r\n";
  q[24].answer = 2;
  q[24].used = 0;
  q[24].categories = "General";
  //question 26
  q[25].qid = 25;
  q[25].question = "Which of these is the site of a famous airfield?\r\n";
  q[25].choice[0] = "Biggin Bay\r\n";
  q[25].choice[1] = "Biggin Lake\r\n";
  q[25].choice[2] = "Biggin Glacier\r\n";
  q[25].choice[3] = "Biggen Hill\r\n";
  q[25].answer = 4;
  q[25].used = 0;
  q[25].categories = "General";
  //question 27
  q[26].qid = 26;
  q[26].question = "Which of these types of paint is also a name for an artificial fabric?\r\n";
  q[26].choice[0] = "Acrylic\r\n";
  q[26].choice[1] = "Emulsion\r\n";
  q[26].choice[2] = "Gloss\r\n";
  q[26].choice[3] = "Undercoat\r\n";
  q[26].answer = 1;
  q[26].used = 0;
  q[26].categories = "General";
  //question 28
  q[27].qid = 27;
  q[27].question = "Who became vice president of the United States in 1993?\r\n";
  q[27].choice[0] = "Al Gore\r\n";
  q[27].choice[1] = "Walter Mondale\r\n";
  q[27].choice[2] = "George W Bush\r\n";
  q[27].choice[3] = "Dick Cheney\r\n";
  q[27].answer = 1;
  q[27].used = 0;
  q[27].categories = "History";
  //question 29
  q[28].qid = 28;
  q[28].question = "Which of these fictional characters sailed on a ship called the Hopewell?\r\n";
  q[28].choice[0] = "Jim Hawkins\r\n";
  q[28].choice[1] = "Captain Ahab\r\n";
  q[28].choice[2] = "Lemuel Gulliver\r\n";
  q[28].choice[3] = "David Balfour\r\n";
  q[28].answer = 3;
  q[28].used = 0;
  q[28].categories = "General";
  //question 30
  q[29].qid = 29;
  q[29].question = "What does 'WAG' mean?\r\n";
  q[29].choice[0] = "Wife\r\n";
  q[29].choice[1] = "Girlfriend\r\n";
  q[29].choice[2] = "Wives and Girlfriends\r\n";
  q[29].choice[3] = "Mother\r\n";
  q[29].answer = 3;
  q[29].used = 0;
  q[29].categories = "General";
  //question 31
  q[30].qid = 30;
  q[30].question = "Which US TV comedy featured a coffee shop manager called Gunther?\r\n";
  q[30].choice[0] = "Will and Grace\r\n";
  q[30].choice[1] = "Frasier\r\n";
  q[30].choice[2] = "Suddenly Susan\r\n";
  q[30].choice[3] = "Friends\r\n";
  q[30].answer = 4;
  q[30].used = 0;
  q[30].categories = "General";
  //question 32
  q[31].qid = 31;
  q[31].question = "Which of these is a kind of lizard?\r\n";
  q[31].choice[0] = "Skink\r\n";
  q[31].choice[1] = "Skerry\r\n";
  q[31].choice[2] = "Skein\r\n";
  q[31].choice[3] = "Skeet\r\n";
  q[31].answer = 1;
  q[31].used = 0;
  q[31].categories = "General";
  //question 33
  q[32].qid = 32;
  q[32].question = "'Alva' was the middle name of which inventor?\r\n";
  q[32].choice[0] = "Thomas Edison\r\n";
  q[32].choice[1] = "Isaac Singer\r\n";
  q[32].choice[2] = "Benjamin Franklin\r\n";
  q[32].choice[3] = "Samuel Morse\r\n";
  q[32].answer = 1;
  q[32].used = 0;
  q[32].categories = "History";
  //question 34
  q[33].qid = 33;
  q[33].question = "What is the title of Dan Brown's best-selling book?\r\n";
  q[33].choice[0] = "The Michelangelo\r\n";
  q[33].choice[1] = "The Raphael Conundrum\r\n";
  q[33].choice[2] = "The Botticelli Cipher\r\n";
  q[33].choice[3] = "Da Vinci Code\r\n";
  q[33].answer = 4;
  q[33].used = 0;
  q[33].categories = "General";
  //question 35
  q[34].qid = 34;
  q[34].question = "Who was the first solo non-stop flight around the world?\r\n";
  q[34].choice[0] = "Steve Fossett\r\n";
  q[34].choice[1] = "Brian Jones\r\n";
  q[34].choice[2] = "Bertrand Piccard\r\n";
  q[34].choice[3] = "Richard Branson\r\n";
  q[34].answer = 1;
  q[34].used = 0;
  q[34].categories = "History";
  //question 36
  q[35].qid = 35;
  q[35].question = "Which of these is at the southern tip of the Malay Peninsula?\r\n";
  q[35].choice[0] = "Yemen\r\n";
  q[35].choice[1] = "Chile\r\n";
  q[35].choice[2] = "Somalia\r\n";
  q[35].choice[3] = "Singapore\r\n";
  q[35].answer = 4;
  q[35].used = 0;
  q[35].categories = "General";
  //question 37
  q[36].qid = 36;
  q[36].question = "The Lord of the Rings', who is played by Orlando Bloom?\r\n";
  q[36].choice[0] = "Legolas\r\n";
  q[36].choice[1] = "Aragorn\r\n";
  q[36].choice[2] = "Boromir\r\n";
  q[36].choice[3] = "Gimli\r\n";
  q[36].answer = 1;
  q[36].used = 0;
  q[36].categories = "General";
  //question 38
  q[37].qid = 37;
  q[37].question = "The campus of which American university is in Cambridge, Massachusetts?\r\n";
  q[37].choice[0] = "Princeton\r\n";
  q[37].choice[1] = "Harvard\r\n";
  q[37].choice[2] = "Yale\r\n";
  q[37].choice[3] = "Columbia\r\n";
  q[37].answer = 2;
  q[37].used = 0;
  q[37].categories = "General";
  //question 39
  q[38].qid = 38;
  q[38].question = "Which of these US states does not have a coastline on the Gulf of Mexico?\r\n";
  q[38].choice[0] = "New Mexico\r\n";
  q[38].choice[1] = "Florida\r\n";
  q[38].choice[2] = "Mississippi\r\n";
  q[38].choice[3] = "Texas\r\n";
  q[38].answer = 1;
  q[38].used = 0;
  q[38].categories = "General";
  //question 40
  q[39].qid = 39;
  q[39].question = "Which symbol appears on the national flag of India?\r\n";
  q[39].choice[0] = "Bird\r\n";
  q[39].choice[1] = "Star\r\n";
  q[39].choice[2] = "Mountain\r\n";
  q[39].choice[3] = "Wheel\r\n";
  q[39].answer = 4;
  q[39].used = 0;
  q[39].categories = "General";
  //question 41
  q[40].qid = 40;
  q[40].question = "Which of these men did not serve as a US president?\r\n";
  q[40].choice[0] = "Thomas Jefferson\r\n";
  q[40].choice[1] = "Theodore Roosevelt\r\n";
  q[40].choice[2] = "Douglas MacArthur\r\n";
  q[40].choice[3] = "Woodrow Wilson\r\n";
  q[40].answer = 3;
  q[40].used = 0;
  q[40].categories = "History";
  //question 42
  q[41].qid = 41;
  q[41].question = "Who plays the role of Doctor Who's assistant Rose Tyler in the new TV series?\r\n";
  q[41].choice[0] = "Anna Friel\r\n";
  q[41].choice[1] = "Billie Piper\r\n";
  q[41].choice[2] = "Rachael Stirling\r\n";
  q[41].choice[3] = "Hannah Waterman\r\n";
  q[41].answer = 2;
  q[41].used = 0;
  q[41].categories = "General";
  //question 43
  q[42].qid = 42;
  q[42].question = "Which of these US states shares a border with Texas?\r\n";
  q[42].choice[0] = "Oklahoma\r\n";
  q[42].choice[1] = "Florida\r\n";
  q[42].choice[2] = "Minnesota\r\n";
  q[42].choice[3] = "Montana\r\n";
  q[42].answer = 1;
  q[42].used = 0;
  q[42].categories = "General";
  //question 44
  q[43].qid = 43;
  q[43].question = "Which of these countries is most associated with the production of coffee?\r\n";
  q[43].choice[0] = "Poland\r\n";
  q[43].choice[1] = "Japan\r\n";
  q[43].choice[2] = "Norway\r\n";
  q[43].choice[3] = "Brazil\r\n";
  q[43].answer = 4;
  q[43].used = 0;
  q[43].categories = "General";
  //question 45
  q[44].qid = 44;
  q[44].question = "A number one followed by one hundred zeros is known by what name?\r\n";
  q[44].choice[0] = "Megatron\r\n";
  q[44].choice[1] = "Gigabit\r\n";
  q[44].choice[2] = "Googol\r\n";
  q[44].choice[3] = "Nanomole\r\n";
  q[44].answer = 3;
  q[44].used = 0;
  q[44].categories = "General";
  //question 46
  q[45].qid = 45;
  q[45].question = "What is the Capitol of California?\r\n";
  q[45].choice[0] = "San Francisco\r\n";
  q[45].choice[1] = "Benicia\r\n";
  q[45].choice[2] = "Sacramento\r\n";
  q[45].choice[3] = "Los Angeles\r\n";
  q[45].answer = 3;
  q[45].used = 0;
  q[45].categories = "General";
  //question 47
  q[46].qid = 46;
  q[46].question = "Where would a cowboy wear his chaps?\r\n";
  q[46].choice[0] = "Hands\r\n";
  q[46].choice[1] = "Legs\r\n";
  q[46].choice[2] = "Arms\r\n";
  q[46].choice[3] = "Head\r\n";
  q[46].answer = 2;
  q[46].used = 0;
  q[46].categories = "General";
  //question 48
  q[47].qid = 47;
  q[47].question = "Sherpas and Gurkhas are native to which country?\r\n";
  q[47].choice[0] = "Ecuador\r\n";
  q[47].choice[1] = "Morocco\r\n";
  q[47].choice[2] = "Russia\r\n";
  q[47].choice[3] = "Nepal\r\n";
  q[47].answer = 4;
  q[47].used = 0;
  q[47].categories = "General";
  //question 49
  q[48].qid = 48;
  q[48].question = "Complete the title of the James Bondfilm The Man With The Golden...\r\n";
  q[48].choice[0] = "Gun\r\n";
  q[48].choice[1] = "Tooth\r\n";
  q[48].choice[2] = "Delicious\r\n";
  q[48].choice[3] = "Eagle\r\n";
  q[48].answer = 1;
  q[48].used = 0;
  q[48].categories = "General";
  //question 50
  q[49].qid = 49;
  q[49].question = "Poaching involves cooking something in what?\r\n";
  q[49].choice[0] = "Beer\r\n";
  q[49].choice[1] = "Oven\r\n";
  q[49].choice[2] = "Liquid\r\n";
  q[49].choice[3] = "Grill\r\n";
  q[49].answer = 3;
  q[49].used = 0;
  q[49].categories = "General";
}
