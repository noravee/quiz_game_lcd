#include "stm32f4xx_hal.h"
#include <string.h>

extern UART_HandleTypeDef huart1;
extern uint8_t sendbuf[256];

int getKeypad(int n){
    uint8_t recvbuf[n+1];
    int keyPad = 0;
    int notNumber = 0;
    for (size_t i = 0; i < n; i++) {
        uint8_t *p = recvbuf + i;

        while(1){
              HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4, GPIO_PIN_SET);
              HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2, GPIO_PIN_RESET);
              HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_RESET);
              HAL_Delay(1);
              if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3)){
                  *p = '1';
                  *(p+1) = '\0';
                  break;
              }else if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_4)){
                  *p = '4';
                  *(p+1) = '\0';
                  break;
              }else if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_3)){
                  *p = '7';
                  *(p+1) = '\0';
                  break;
              }else if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)){
                  *p = '*';
                  *(p+1) = '\0';
                  notNumber = 1;
                  break;
              }

              HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4, GPIO_PIN_RESET);
              HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2, GPIO_PIN_SET);
              HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_RESET);
              HAL_Delay(1);
              if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3)){
                  *p = '2';
                  *(p+1) = '\0';
                  break;
              }else if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_4)){
                  *p = '5';
                  *(p+1) = '\0';
                  break;
              }else if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_3)){
                  *p = '8';
                  *(p+1) = '\0';
                  break;
              }else if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)){
                  *p = '0';
                  *(p+1) = '\0';
                  break;
              }

              HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4, GPIO_PIN_RESET);
              HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2, GPIO_PIN_RESET);
              HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_SET);
              HAL_Delay(1);
              if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3)){
                  *p = '3';
                  *(p+1) = '\0';
                  break;
              }else if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_4)){
                  *p = '6';
                  *(p+1) = '\0';
                  break;
              }else if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_3)){
                  *p = '9';
                  *(p+1) = '\0';
                  break;
              }else if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)){
                  *p = '#';
                  *(p+1) = '\0';
                  notNumber = 1;
                  break;
              }
        }
        snprintf((char *) sendbuf, 2, "%c", (char)*p);
        // displayStringOnLCDAtLine(sendbuf, 4, 21);
        // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
        HAL_Delay(300);
    }
    if(notNumber == 0){
        sscanf((char *) recvbuf, "%d", &keyPad);
    }
    else{
      keyPad = -1;
    }
    return keyPad;
}
