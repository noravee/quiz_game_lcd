#include <stdlib.h>
#include <string.h>

#include "huzzah.h"
#include "stm32f4xx_hal.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart5;

volatile uint8_t flag = 0;

void huzzah_send_command3(char *command, int command_len, int resc, char *resv, uint8_t mode){
  int len;
  if (!mode){
      if (resc > 0){
          len = command_len + resc + 4;
      } else {
          len = command_len + 2;
      }
  } else {
      if (resc > 0){
          len = command_len + resc + 2;
      }
  }

  uint8_t buf[len];
  for(int i = 0; i<len; i++) buf[i] = '\0';
  flag = 0;

  HAL_UART_Receive_DMA(&huart5, buf, len);
  HAL_UART_Transmit_DMA(&huart5, (uint8_t *) command, command_len);
  while(!flag){}
  for (int i = 0; i<resc; i++){
      if (!mode){
          resv[i] = buf[command_len + i];
      } else {
          resv[i] = buf[command_len + i + 2];
      }
  }
  //debug("\r\nBEGIN\r\n");
  //HAL_UART_Transmit(&huart1, (uint8_t *) buf, len, 10);
  //debug("\r\nEND\r\n");
  HAL_Delay(50);
}

void huzzah_send_command2(char *command, int resc, char *resv, uint8_t mode){
  int command_len = strlen(command);
  huzzah_send_command3(command, command_len, resc, resv, mode);
}

void huzzah_send_command(char *command, int resc, char *resv){
    huzzah_send_command2(command, resc, resv, 0);
}

int huzzah_wifi_status(){
    char c[2] = {'\0'};
    huzzah_send_command("print(wifi.sta.status())\r\n", 1, c);
    return atoi(c);
}

uint8_t huzzah_await_connection(){
    uint8_t status;
    while (1){
        status = huzzah_wifi_status();
        switch (status){
            case 2:
            case 3:
            case 4:
                return 0;
            case 5:
                return 1;
        }
    }
}

void huzzah_connect_station(char *ssid, char *password){
    huzzah_send_command("wifi.setmode(wifi.STATION)\r\n", 0, NULL);
    char command[128];
    snprintf(command, 128, "wifi.sta.config(\"%s\", \"%s\", 1)\r\n", ssid, password);
    huzzah_send_command(command, 0, NULL);
}

void huzzah_http_get(char *host, char *url, int port, int resc, char *resv){
    huzzah_send_command("sk=net.createConnection(net.TCP, 0)\r\n", 0, NULL);
    huzzah_send_command("sk:on(\"receive\", function(sck, c) uart.write(0, c) end )\r\n", 0, NULL);

    char command[64];
    snprintf(command, 64, "sk:connect(%d,\"%s\")\r\n", port, host);
    huzzah_send_command(command, 0, NULL);

    snprintf(command, 64, "sk:send(\"GET %s HTTP/1.0\\r\\nHost: %s:%d\\r\\n\\r\\n\")\r\n", url, host, port);

    huzzah_send_command2(command, resc, resv, 1);
    huzzah_send_command("sk:close()\r\n", 0, NULL);
}

void huzzah_http_post(char *host, char *url, int port, int resc, char *resv, char *content, uint32_t contentLength){
    huzzah_send_command("sk=net.createConnection(net.TCP, 0)\r\n", 0, NULL);
    huzzah_send_command("sk:on(\"receive\", function(sck, c) uart.write(0, c) end )\r\n", 0, NULL);

    char command[60001] = {'0'};
    snprintf(command, 64, "sk:connect(%d,\"%s\")\r\n", port, host);
    huzzah_send_command(command, 0, NULL);
    snprintf(command, 60000, "sk:send(\"POST %s HTTP/1.0\\r\\nHost: %s:%d\\r\\nContent-Type: image/jpg;\\r\\nContent-Length: %d\\r\\n\\r\\n%s\")\r\n", url, host, port, 4, "data");

    snprintf(command, 50000, "sk:send(\"POST %s HTTP/1.0\\r\\nHost: %s:%d\\r\\nContent-Type: image/jpg;\\r\\nContent-Length: %d\\r\\n\\r\\n", url, host, port, contentLength);

    int command_len = strlen(command);

    memcpy(&command[command_len], content, contentLength);
    command[command_len + contentLength] = '\"';
    command[command_len + contentLength] = ')';
    command[command_len + contentLength] = '\r';
    command[command_len + contentLength] = '\n';

    // huzzah_send_command2(command, resc, resv, 1);
    huzzah_send_command3(command, command_len, resc, resv, 1);
    huzzah_send_command("sk:close()\r\n", 0, NULL);
}
