#include "stm32f429i_discovery_lcd.h"

#define TIMEOUT  0.5    // I needed a longer timeout than ladyada's 0.2 value
#define SERIALNUM 0    // start with 0, each camera should have a unique ID.

#define COMMANDSEND 0x56
#define COMMANDREPLY 0x76
#define COMMANDEND 0x00
#define CMD_GETVERSION  0x11
#define CMD_RESET  0x26
#define CMD_TAKEPHOTO  0x36
#define CMD_READBUFF  0x32
#define CMD_GETBUFFLEN  0x34
#define FBUF_CURRENTFRAME  0x00
#define FBUF_NEXTFRAME  0x01

#define FBUF_STOPCURRENTFRAME 0x00

extern UART_HandleTypeDef huart1;

uint8_t getversioncommand[] = {COMMANDSEND, SERIALNUM, CMD_GETVERSION, COMMANDEND};
uint8_t resetcommand[] = {COMMANDSEND, SERIALNUM, CMD_RESET, COMMANDEND};
uint8_t takephotocommand[] = {COMMANDSEND, SERIALNUM, CMD_TAKEPHOTO, 0x01, FBUF_STOPCURRENTFRAME};
uint8_t getbufflencommand[] = {COMMANDSEND, SERIALNUM, CMD_GETBUFFLEN, 0x01, FBUF_CURRENTFRAME};
uint8_t readphotocommand[] = {COMMANDSEND, SERIALNUM, CMD_READBUFF, 0x0c, FBUF_CURRENTFRAME, 0x0a};


int cameraReset(){
    HAL_UART_Transmit(&huart1, resetcommand, 4, 100);
    uint8_t recv[100]={'a','a','a','a'};
    HAL_UART_Receive(&huart1, recv, 100, 1000);

    char buf[16];
    //snprintf(buf, 16, "%d %d %d %d", recv[0], recv[1], recv[2], recv[3]);

    //BSP_LCD_DisplayStringAt(0, 50, (uint8_t*) buf, CENTER_MODE);

    if (recv[0] == COMMANDREPLY && recv[2] == CMD_RESET && recv[3] == 0x0){
        return 1;
    }
    return 0;
}

int cameraGetVersion(){
  HAL_UART_Transmit(&huart1, getversioncommand, 4, 100);
  uint8_t recv[16]={'a','a','a','a'};
  HAL_UART_Receive(&huart1, recv, 16, 100);
  if (recv[0] == COMMANDREPLY && recv[2] == CMD_GETVERSION && recv[3] == 0x0){
      return 1;
  }
	return 0;
}

int cameraTakePhoto(){
    HAL_UART_Transmit(&huart1, takephotocommand, 5, 100);
    uint8_t recv1[5]={'a','a','a','a','a'};
    HAL_UART_Receive(&huart1, recv1, 5, 100);
    char buf[16];
    //snprintf(buf, 16, "%x %x %x %x %x", recv1[0], recv1[1], recv1[2], recv1[3], recv1[4]);

    //BSP_LCD_DisplayStringAt(0, 300, (uint8_t*) buf, CENTER_MODE);

    if (recv1[0] == COMMANDREPLY && recv1[2] == CMD_TAKEPHOTO && recv1[3] == 0x0){
        return 1;
    }
    return 0;
}

uint32_t getbufferlength(){
    HAL_UART_Transmit(&huart1, getbufflencommand, 5, 100);
    uint8_t recv2[9]={'a','a','a','a','a','a','a','a','a'};
    HAL_UART_Receive(&huart1, recv2, 9, 100);
    // char buf[16];
    // snprintf(buf, 16, "%x %x %x %x %x", recv2[0], recv2[1], recv2[2], recv2[3], recv2[4]);
    //
    // BSP_LCD_DisplayStringAt(0, 130, (uint8_t*) buf, CENTER_MODE);
    if( recv2[0] == COMMANDREPLY && recv2[2] == CMD_GETBUFFLEN && recv2[3] == 0x0 && recv2[4] == 0x4){
        //buf[16];
        // snprintf(buf, 16, "%x %x %x %x", recv2[5], recv2[6], recv2[7], recv2[8]);
        //
        // BSP_LCD_DisplayStringAt(0, 160, (uint8_t*) buf, CENTER_MODE);

        uint8_t l[4] = {recv2[8], recv2[7], recv2[6], recv2[5]};

        //uint32_t *l = (uint32_t *)&recv2[5];

        //uint8_t vals[] = {recv2[8], recv2[7], recv2[6], recv2[5]};
        //uint8_t vals[] = {recv2[5], recv2[6], recv2[7], recv2[8]};
        uint32_t bytes = *((uint32_t*) l);

        char a[16];
        //snprintf(a, 16, "%lu", *((uint32_t*) l));
        //BSP_LCD_DisplayStringAt(0, 100, (uint32_t*) a, CENTER_MODE);

        return bytes;
    }
    return 0;
}

void readbuffer(uint32_t bytes, uint8_t *photo){

  uint32_t addr = 0;   // the initial offset into the frame buffer
  //uint32_t bytes = *((uint32_t*) a);
  char b[16];
  // snprintf(b, 16, "%lu", bytes);
  // BSP_LCD_DisplayStringAt(0, 160, (uint32_t*) b, CENTER_MODE);

  int pIndex = 0;

	// bytes to read each time (must be a mutiple of 4)
	uint32_t inc = 1024;

//while( addr < (bytes+32) ){
  while( addr < (bytes) ){
 		// on the last read, we may need to read fewer bytes.
    //uint32_t chunk = min( bytes-addr, inc );
    uint32_t chunk;
    if ((bytes-addr) <= inc){
        chunk = bytes-addr;
    }else{
        chunk = inc;
    }

		// append 4 bytes that specify the offset into the frame buffer
		//uint8_t command[10] = readphotocommand + [(addr >> 24) & 0xff,(addr>>16) & 0xff,(addr>>8 ) & 0xff,addr & 0xff];
    uint8_t command[16];
    //uint8_t readphotocommand[] = {COMMANDSEND, SERIALNUM, CMD_READBUFF, 0x0c, FBUF_CURRENTFRAME, 0x0a};
    command[0] = COMMANDSEND;
    command[1] = SERIALNUM;
    command[2] = CMD_READBUFF;
    command[3] = 0x0c;
    command[4] = FBUF_CURRENTFRAME;
    command[5] = 0x0a;
    command[6] = (addr >> 24) & 0xff;
    command[7] = (addr>>16) & 0xff;
    command[8] = (addr>>8 ) & 0xff;
    command[9] = addr & 0xff;

		// append 4 bytes that specify the data length to read
		//command += [(chunk >> 24) & 0xff,(chunk>>16) & 0xff,(chunk>>8 ) & 0xff,chunk & 0xff];
    command[10] = (chunk >> 24) & 0xff;
    command[11] = (chunk>>16) & 0xff;
    command[12] = (chunk>>8 ) & 0xff;
    command[13] = chunk & 0xff;
    // command[10] =  (bytes >> 24) & 0xff ;
    // command[11] =  (bytes >> 16) & 0xff ;
    // command[12] =  (bytes >> 8) & 0xff ;
    // command[13] =  (bytes) & 0xff;
    // command[10] =  0 ;
    // command[11] =  0 ;
    // command[12] =  0 ;
    // command[13] =  32;
		// append the delay
		//command += [1,0];
    command[14] = 1;
    command[15] = 0;

		//print "Reading", chunk, "bytes at", addr

		// make a string out of the command bytes.
    HAL_UART_Transmit(&huart1, command,16, 100);

		// the reply is a 5-byte header, followed by the image data
		//   followed by the 5-byte header again.
    uint8_t recv3[5+chunk+5];
    //HAL_UART_Receive(&huart1, recv3, 9, 200);
    //uint8_t recv3[bytes+5];
    //uint8_t recv3[32+5+5];

    if (HAL_UART_Receive(&huart1, recv3, chunk+5+5, 1000) == HAL_TIMEOUT){
        //BSP_LCD_DisplayStringAt(0, 190, (uint8_t*) "Time out", CENTER_MODE);
    }
    ////HAL_UART_Transmit(&huart1, recv3+5, 32, 1000);

    //HAL_UART_Transmit(&huart1, recv3+5, chunk, 1000);

    if ((sizeof(recv3)/sizeof(recv3[0])) != 5+chunk+5){
        continue;
    }

		// if( len(r) != 5+chunk+5 ):
		// 	// retry the read if we didn't get enough bytes back.
		// 	print "Read", len(r), "Retrying."
		// 	continue

    char buf1[16];
    // snprintf(buf1, 16, "%x %x %x %x %x", recv3[0], recv3[1], recv3[2], recv3[3], recv3[4]);
    //
    // BSP_LCD_DisplayStringAt(0, 100, (uint8_t*) buf1, CENTER_MODE);

    if (!(recv3[0] == COMMANDREPLY && recv3[2] == CMD_READBUFF && recv3[3] == 0x0)){
        //BSP_LCD_DisplayStringAt(0, 190, (uint8_t*) "ERROR READING PHOTO", CENTER_MODE);
        //BSP_LCD_DisplayStringAt(0, 220, (uint8_t*) "ERROR READ", CENTER_MODE);
        //return;
    }

		// append the data between the header data to photo
    for (int i=5;i<=chunk+5;i++){
        photo[pIndex] = recv3[i];
        pIndex++;
    }
    // for (int i=5;i<=(32+5);i++){
    //     photo[pIndex] = recv3[i];
    //     pIndex++;
    // }

		// advance the offset into the frame buffer
		addr += chunk;
    //addr += 32;
 }

  BSP_LCD_DisplayStringAt(0, 250, (uint8_t*) "Bytes written", CENTER_MODE);
  //HAL_UART_Transmit(&huart1, (uint8_t *)photo, bytes, 1000);
  //HAL_UART_Transmit(&huart1, (uint8_t *)photo, sizeof(photo), 1000);


  //char buf3[16];
  // snprintf(buf3, 16, "%lu", *(uint32_t*)photo);
  // BSP_LCD_DisplayStringAt(0, 130,(uint32_t*) buf3 , CENTER_MODE);
	//return &photo;
}

void getPhoto(){

  if (cameraReset() == 1){
      BSP_LCD_DisplayStringAt(0, 20, (uint8_t *) "reset complete", CENTER_MODE);
  }
  else{
      BSP_LCD_DisplayStringAt(0, 20, (uint8_t *) "reset fail", CENTER_MODE);
  }

  HAL_Delay(500);

  if (!cameraGetVersion()){
      BSP_LCD_DisplayStringAt(0, 20, (uint8_t *) "version fail", CENTER_MODE);
  }

  HAL_Delay(500);
  if (cameraTakePhoto() == 1){
      BSP_LCD_DisplayStringAt(0, 80, (uint8_t *) "take photo complete", CENTER_MODE);
  }
  else{
      BSP_LCD_DisplayStringAt(0, 80, (uint8_t *) "take photo fail", CENTER_MODE);
  }

  uint32_t bytes = getbufferlength();

  if (bytes == 0){
      BSP_LCD_DisplayStringAt(0, 130, (uint8_t *) "getbuff fail", CENTER_MODE);
  }
  else{
      //BSP_LCD_DisplayStringAt(0, 130, (uint8_t *)"getbuff complete" , CENTER_MODE);
      // char a[16];
      // snprintf(a, 16, "%lu", bytes);
      // BSP_LCD_DisplayStringAt(0, 160,(uint32_t*) a , CENTER_MODE);

  }
  //readbuffer(bytes);
  uint8_t photo[bytes];
  readbuffer(bytes, photo);
  //photo2 = photo;
  HAL_UART_Transmit(&huart1, (uint8_t *)photo, sizeof(photo), 1000);
  // char buf3[16];
  // snprintf(buf3, 16, "%lu", photo);
  // BSP_LCD_DisplayStringAt(0, 130,(uint8_t*) buf3 , CENTER_MODE);
  //return photo;
}
