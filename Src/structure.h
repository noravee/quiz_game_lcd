#ifndef STRUCTURE_H
#define STRUCTURE_H

struct Player{
  int pid;
  char name[10];
  int score;
};

struct questions{
  int qid;
  char *question;
  char *choice[4];
  int answer;
  int used;
  char *categories;
};

#endif
