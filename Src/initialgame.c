#ifndef MAX_PLAYER
#define MAX_PLAYER 8
#endif

#include "stm32f429i_discovery_lcd.h"
#include <string.h>

#include "structure.h"

extern UART_HandleTypeDef huart1;
extern uint8_t sendbuf[256];
extern uint8_t recvbuf[15];
extern struct Player player[MAX_PLAYER];
extern int np;

void generateQuestionDatabase();
int getKeypad(int n);
void getKeyboard(int n);
/// Initial Game function////
void initGame(){
  BSP_LCD_SelectLayer(0);
  BSP_LCD_Clear(LCD_COLOR_RED);
  BSP_LCD_SetFont(&Font16);
  BSP_LCD_ClearStringLine(2);
  // snprintf((char *) sendbuf, 256, "Generating Question Database!\r\n");
  // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 100);
  generateQuestionDatabase();
  // snprintf((char *) sendbuf, 256, "Database ready!\r\nGame Initialized!\r\n");
  // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 100);

  char *text = "Please enter number of player (1-8 players): ";
  int lastLine = displayStringOnLCDAtLine(text, 1, 21);
  // snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
  // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
  np = getKeypad(1);
  snprintf((char *)sendbuf, 4, "%d  ", np);
  lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
  HAL_Delay(300);
  //printf("%d\n",np);
  if(np>8||np<=0){
    text = "\r\nPlease read instruction and enter number of player again!!!\r\n";
    displayStringOnLCDAtLine(text, lastLine+2, 21);
    // snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
    // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
    initGame();
  }

  else{
    // struct Player player[np];
    for (int i=0;i<np;i++){
      player[i].pid=i+1;
      player[i].score=0;
      snprintf((char *) sendbuf, 255, "Player %d", i+1);
      strncpy(player[i].name, sendbuf, strlen(sendbuf));
      // snprintf((char *) sendbuf, 255, "\r\nPlayer[%d]\t", player[i].pid);
      // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
      // printf("name (max 10 characters): ");
      // snprintf((char *) sendbuf, 255, "name (max 10 characters): ");
      // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
      // getKeyboard(10);
      // strncpy(player[i].name, (char *)recvbuf, strlen((char *)recvbuf));
      // if(strlen(player[i].name)>10||strlen(player[i].name)<=0){
      //   printf("Please read instruction and enter player[%d] name agian!!!\n",player[i].pid);
      //   i=i-1;
      // }
      }
  /////////////////////////////

  ////show all player and Initial score////
    snprintf((char *) sendbuf, 255, "\r\nAll player and initial score\r\n");
    // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
    BSP_LCD_Clear(LCD_COLOR_RED);
    snprintf((char *) sendbuf, 255, "All player and initial score  ");
    lastLine = displayStringOnLCDAtLine(sendbuf, 1, 21);
    for (int j=0;j<np;j++){
      snprintf((char *) sendbuf, 255, "Player[%d]\t",player[j].pid);
      // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
      lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
      snprintf((char *) sendbuf, 255, "name : %s\t",player[j].name);
      // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
      lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
      snprintf((char *) sendbuf, 255, "score: %d\r\n",player[j].score);
      // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
      lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
    }
  }

  startGame();
}
////////////////////////////////
