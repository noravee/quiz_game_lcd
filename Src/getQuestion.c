#ifndef NUMBER_OF_QUESTION
#define NUMBER_OF_QUESTION 50
#endif

#include <string.h>
#include <stdlib.h>
#include "stm32f429i_discovery_lcd.h"
#include "structure.h"

extern UART_HandleTypeDef huart1;
extern struct questions q[NUMBER_OF_QUESTION];
extern uint8_t sendbuf[256];

int displayStringOnLCDAtLine(char *text, int startLine, int charPerLine);

unsigned int randomr(unsigned int min, unsigned int max)
{
       double x= (double)rand()/RAND_MAX;
       return (max - min +1)*x+ min;
}

int getQuestion()
{
    int r = -1;
    while(1){
        // r = rand() % NUMBER_OF_QUESTION;
        r = rand() % NUMBER_OF_QUESTION;
        // r = rand() / (RAND_MAX / NUMBER_OF_QUESTION + 1);
        r = randomr(0, NUMBER_OF_QUESTION-1);
        if(q[r].used == 0){
            break;
        }
    }
    q[r].used = 1;
    snprintf((char *) sendbuf, strlen(q[r].question)+1, "%s", q[r].question);
    HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
    BSP_LCD_SelectLayer(0);
  	// BSP_LCD_Clear(LCD_COLOR_RED);
    BSP_LCD_SetFont(&Font16);
    for (size_t i = 2; i < 17; i++) {
      BSP_LCD_ClearStringLine(i);
    }
    BSP_LCD_ClearStringLine(2);
    int lastLine = displayStringOnLCDAtLine(q[r].question, 2, 21);
    lastLine++;
    for (size_t i = 0; i < 4; i++) {
        snprintf((char *) sendbuf, strlen(q[r].choice[i])+4+1, "%d-%s", i+1, q[r].choice[i]);
        HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
        lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
    }

    return q[r].qid;
}
