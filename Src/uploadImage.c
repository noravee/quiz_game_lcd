#include <string.h>
#include "dummyphoto.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart5;

void debug(char *msg);

// void uploadImage(uint8_t *photo, uint32_t bytes)
void uploadImage()
{
  uint8_t *photo = photo_jpg;
  uint32_t bytes = photo_jpg_len;
  char photoHexString[bytes *2 + 1];

  for (int i = 0; i < bytes; i++) {
    snprintf(photoHexString + 2*i, 3, "%02X", photo[i]);
  }
  huzzah_connect_station("Pi3-AP", "raspberry");
  HAL_Delay(500);
  if(!huzzah_await_connection()){
    HAL_Delay(200);
      // debug("Failed to connect to wifi.\r\n");
  }
  // debug("Connected to wifi.\r\n");

  char bufwifi[1024];
  char *txt = "test";
  unsigned char image[] = {0xff, 0xd8, 0xff, 0xfe, 0x00};
  int image_len = 5;

  // debug("Send request\r\n");
  memset(bufwifi, '\0', 1024);
  // huzzah_http_get("172.24.1.113", "/", 5000, 1, bufwifi);
  // huzzah_http_get("192.168.43.80", "/", 5000, 167, bufwifi);
  // huzzah_http_get("192.168.43.182", "/", 5000, 167, bufwifi);
  // debug("GET Respone:\r\n");
  // debug(bufwifi);
  // debug("\r\n\r\n");
  // huzzah_http_post("172.24.1.113", "/", 5000, 159, bufwifi, "image", 5);
  // huzzah_http_post("192.168.43.80", "/", 5000, 160, bufwifi, "image", 5);
  // debug("POST Respone:\r\n");
  // debug(bufwifi);
  // debug("\r\n\r\n");

  // huzzah_http_post("172.24.1.113", "/", 5000, 159, bufwifi, photo_jpg, photo_jpg_len);
  // huzzah_http_post("172.24.1.113", "/", 5000, 159, bufwifi, image, image_len);
  // huzzah_http_post("172.24.1.113", "/", 5000, 159, bufwifi, photoHexString, strlen(photoHexString));
  // huzzah_http_post("192.168.43.182", "/", 5000, 159, bufwifi, photoHexString, strlen(photoHexString));
  huzzah_http_post("192.168.43.80", "/", 5000, 159, bufwifi, photoHexString, strlen(photoHexString));

  char find = '#';
  int result, answer;
  sscanf(bufwifi, "#%d %d", &result, &answer);
  const char *ptr = strchr(bufwifi, find);
  if(ptr) {
     int index = ptr - bufwifi;
     debug((char *)ptr);
     debug((char *)bufwifi[index]);
  }

  HAL_Delay(100);
}
