#include <string.h>
#include <math.h>
#include "stm32f429i_discovery_lcd.h"

extern UART_HandleTypeDef huart1;
extern uint8_t sendbuf[256];

int displayStringOnLCDAtLine(char *text, int startLine, int charPerLine)
{
  uint8_t buffer[charPerLine+1];
  int length = strlen(text);
  int i = 0;
  for (i = 0; i < ceil(strlen(text)/(double)charPerLine); i++) {
    BSP_LCD_ClearStringLine(startLine+i);
    if(length >= charPerLine){
      snprintf((char *) buffer, charPerLine+1, "%s", text+(i*charPerLine));
    }
    else{
      snprintf((char *) buffer, length-1, "%s", text+(i*charPerLine));
    }
    BSP_LCD_DisplayStringAtLine(startLine+i, buffer);
    length -= charPerLine;
  }
  return i+startLine-1;
}
