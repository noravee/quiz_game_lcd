#include "stm32f429i_discovery_lcd.h"
#include <string.h>

extern UART_HandleTypeDef huart1;
extern uint8_t sendbuf[256];
extern uint8_t recvbuf[15];

void getKeyboard(int n){
    while (1)
    {
        for (uint8_t i = 0; i < n; i++){
            uint8_t *p = recvbuf + i;
            if (HAL_UART_Receive(&huart1, p, 1, 10) == HAL_TIMEOUT){
                i--;
            } else if (*p == '\r') {
                *p = '\0';
                break;
            } else {
                *(p+1) = '\0';
                //snprintf((char *) sendbuf, 64, "%s", p);
                //HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
                HAL_UART_Transmit(&huart1, (char *)p, 1, 10);
            }
        }
        recvbuf[n] = '\0';
        return;
    }
}
