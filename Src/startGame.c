#ifndef MAX_PLAYER
#define MAX_PLAYER 8
#endif

#include <string.h>
#include "stm32f429i_discovery_lcd.h"
#include "structure.h"

extern int np;
extern struct Player player[MAX_PLAYER];
extern UART_HandleTypeDef huart1;
extern uint8_t sendbuf[256];
extern uint8_t recvbuf[15];

int displayStringOnLCDAtLine(char *text, int startLine, int charPerLine);

////////// start game/////////////////////
int playround;
int ans;
int maxScore;
int winnnerCount;
int candidateID[8];

void startGame(){
   playround = 2; ///depend on user//
   ans = 0;
   maxScore = 0;
   winnnerCount = 0;
   int qid=0;
  // candidateID = [9,9,9,9,9,9,9,9];
  int lastLine = -1;
  char *text = "";

    //  Normal state game //
    for(int i=0;i<playround;i++){
        maxScore = 0;
        // printf("\r\n++++++ Round %d ++++++\r\n",i+1);
        snprintf((char *) sendbuf, 255, "\r\n++++++ Round %d ++++++\r\n",i+1);
        // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
        BSP_LCD_SelectLayer(0);
        BSP_LCD_Clear(LCD_COLOR_RED);
        BSP_LCD_SetFont(&Font16);
        snprintf((char *) sendbuf, 10, "Round %d  ",i+1);
        lastLine = displayStringOnLCDAtLine(sendbuf, 0, 21);
        for(int j=0;j<np;j++){
            // printf("\r\n>>>>> %s turn <<<<< \r\n",player[j].name);
            snprintf((char *) sendbuf, 255, "\r\n>>>>> %s's turn <<<<< \r\n",player[j].name);
            // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
            BSP_LCD_ClearStringLine(1);
            snprintf((char *) sendbuf, 255, "%s's turn  ",player[j].name);
            lastLine = displayStringOnLCDAtLine(sendbuf, 1, 21);
            ///run question and check answer//
            qid = getQuestion();
            ans = getKeypad(1);
            // scanf("%d",&ans);
            verifyAnswer(player[j].pid,qid,ans);
            ///update score///
            if(player[j].score >= maxScore){
                maxScore = player[j].score;
            }
        }
        // Show score each round
        // HAL_UART_Transmit(&huart1, "\r\n", 2, 10);
        BSP_LCD_Clear(LCD_COLOR_RED);
        for(int k=0;k<np;k++){
            // printf("%s score = %d\n",player[k].name,player[k].score);
            snprintf((char *) sendbuf, 255, "%s's score = %d\r\n",player[k].name,player[k].score);
            // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
            // not disp
            // lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
        }
        // HAL_UART_Transmit(&huart1, "\r\n", 2, 10);
    }
    //  End normal state game //

    //  check number of winner  //
    //  collect pid of highest score player to candidateID array //
    int index = 0;
    for(int i=0;i<np;i++){
        if(player[i].score == maxScore){
            winnnerCount++;
            candidateID[index] = i;
            index++;
        }
    }
    //printf("winnercount = %d\n",winnnerCount);
    //printf("max score = %d\n",maxScore);
    // HAL_UART_Transmit(&huart1, "\r\n", 2, 10);
    // for(int k=0;k<np;k++){
    //     // printf("%s score = %d\n",player[k].name,player[k].score);
    //     snprintf((char *) sendbuf, 255, "%s score = %d\n",player[k].name,player[k].score);
    //     HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
    // }
    // HAL_UART_Transmit(&huart1, "\r\n", 2, 10);

    //  If there are more than one winner, start golden goal for only highest score players //
    if(winnnerCount > 1){
        //  play the game until there is only on winnner  //
        int k=1;
        while (winnnerCount > 1) {
            maxScore = 0;
            int playerCount = winnnerCount;
            winnnerCount = 0;
            // printf("\r\nr ++++++ Final round %d ++++++\r\n",k);
            snprintf((char *) sendbuf, 255, "\r\n++++++ Final round %d ++++++\r\n",k);
            // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
            // printf("There are %d players left\r\n",playerCount);
            BSP_LCD_Clear(LCD_COLOR_RED);
            lastLine = displayStringOnLCDAtLine("Final Round  ", 0, 21);
            snprintf((char *) sendbuf, 255, "There are %d players left\r\n",playerCount);
            // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
            lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
            for(int j=0;j<playerCount;j++){
                // printf("\r\n>>>>> %s turn <<<<<\r\n",player[candidateID[j]].name);
                snprintf((char *) sendbuf, 255, "\r\n>>>>> %s's turn <<<<<\r\n",player[candidateID[j]].name);
                // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
                snprintf((char *) sendbuf, 255, "%s's turn  ",player[j].name);
                lastLine = displayStringOnLCDAtLine(sendbuf, 1, 21);
                ///run question in final round//
                qid = getQuestion();
                ans = getKeypad(1);
                // scanf("%d",&ans);
                verifyAnswer(player[candidateID[j]].pid,qid,ans);
                ///update max score///
                if(player[candidateID[j]].score >= maxScore){
                    maxScore = player[candidateID[j]].score;
                }
            }
            // HAL_UART_Transmit(&huart1, "\r\n", 2, 10);
            BSP_LCD_Clear(LCD_COLOR_RED);
            for(int k=0;k<np;k++){
                // printf("%s score = %d\n",player[k].name,player[k].score);
                snprintf((char *) sendbuf, 255, "%s score = %d\r\n",player[k].name,player[k].score);
                // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
                lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
            }
            int index = 0;
            for(int i=0;i<playerCount;i++){
                if(player[candidateID[i]].score == maxScore){
                    winnnerCount++;
                    candidateID[index] = candidateID[i];
                    index++;
                }else if(player[candidateID[i]].score != maxScore){
                    printf("%s lose\r\n",player[candidateID[i]].name);
                    snprintf((char *) sendbuf, 255, "%s lose\r\n",player[candidateID[i]].name);
                    // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
                    lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
                }
            }
            k++;
        }
        BSP_LCD_Clear(LCD_COLOR_RED);
        text = "+++ Score Board +++\r\n";
        snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
        // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
        lastLine = displayStringOnLCDAtLine("+ Score Board +  ", 0, 21);
        for(int k=0;k<np;k++){
            // printf("%s score = %d\r\n",player[k].name,player[k].score);
            snprintf((char *) sendbuf, 255, "%s's score = %d\r\n",player[k].name,player[k].score);
            // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
            snprintf((char *) sendbuf, 22, "%s's score = %d  ",player[k].name,player[k].score);
            lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
        }
        text = "+++++++++++++++++++\r\n";
        snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
        // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
        // printf("\r\n%s win\r\n",player[candidateID[0]].name);
        snprintf((char *) sendbuf, 255, "\r\n%s win!!!\r\n",player[candidateID[0]].name);
        snprintf((char *) sendbuf, 255, "%s win!!!\r\n",player[candidateID[0]].name);
        // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
        lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+2, 21);
    }else{
        BSP_LCD_Clear(LCD_COLOR_RED);
        text = "+++ Score Board +++\r\n";
        snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
        // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
        lastLine = displayStringOnLCDAtLine("++ Score Board ++  ", 0, 21);
        for(int k=0;k<np;k++){
            // printf("%s score = %d\r\n",player[k].name,player[k].score);
            snprintf((char *) sendbuf, 255, "%s's score = %d\r\n",player[k].name,player[k].score);
            // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
            snprintf((char *) sendbuf, 22, "%s's score = %d  ",player[k].name,player[k].score);
            lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+1, 21);
        }
        text = "+++++++++++++++++++\r\n";
        snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
        // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
        // printf("%s win\r\n",player[candidateID[0]].name);
        snprintf((char *) sendbuf, 255, "%s win\r\n",player[candidateID[0]].name);
        HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
        lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+2, 21);
    }
    text = "\r\n+++ Hi-Score Board +++\r\n";
    snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
    // not disp
    // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
    for(int k=0;k<np;k++){
        // printf("%s score = %d\r\n",player[k].name,player[k].score);
        snprintf((char *) sendbuf, 255, "%s's score = %d\r\n",player[k].name,player[k].score);
        // not disp
        // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
    }
    text = "End Game.\r\n";
    snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
    // not disp
    // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
    ///check exit or play again//
    checkplay();
}
