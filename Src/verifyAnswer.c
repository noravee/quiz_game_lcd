#ifndef NUMBER_OF_QUESTION
#define NUMBER_OF_QUESTION 50
#endif
#ifndef MAX_PLAYER
#define MAX_PLAYER 8
#endif

#include "structure.h"

extern UART_HandleTypeDef huart1;
extern struct questions q[NUMBER_OF_QUESTION];
extern struct Player player[MAX_PLAYER];
extern uint8_t sendbuf[256];

void verifyAnswer(int pid,int qid,int ans) {
  // int trueAns = 0;
  // for(int i=0; i<sizeof(q)/sizeof(*q); i++){
  //   if (qid == q[i].qid){
  //     if(trueAns == q[i].answer){
  //       break;
  //     }
  //   }
  // }
  // if(trueAns == ans){
  //   for(int j=0; j<sizeof(player)/sizeof(*player); j++){
  //     if(pid == player[j].pid){
  //       player[j].score++;
  //     }
  //   }
  // }
  // return;
  if (ans == q[qid].answer) {
    player[pid-1].score++;
    // HAL_UART_Transmit(&huart1, "\r\nCorrect!\r\n", 12, 10);
    displayStringOnLCDAtLine("Correct!  ", 17, 21);
    HAL_Delay(300);
    BSP_LCD_ClearStringLine(17);
  }
  else{
    // HAL_UART_Transmit(&huart1, "\r\nWrong!\r\n", 10, 10);
    displayStringOnLCDAtLine("Wrong!  ", 17, 21);
    HAL_Delay(300);
    BSP_LCD_ClearStringLine(17);
  }
  return;
}
