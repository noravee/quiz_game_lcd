#ifndef MAX_PLAYER
#define MAX_PLAYER 8
#endif

#include <string.h>
#include "structure.h"
#include "stm32f429i_discovery_lcd.h"
// #include "functions.h"

extern UART_HandleTypeDef huart1;
extern struct Player player[MAX_PLAYER];
extern int np;
extern uint8_t sendbuf[256];

void checkplay() {
  int code;
  char *text = "Exit or play again [1:exit, 2:Play again]:  ";
  snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
  // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
  int lastLine = displayStringOnLCDAtLine(sendbuf, 10, 21);
  // scanf("%d",&code);
  code = getKeypad(1);
  ///check code///
  switch (code){
  case 1: text = "\r\nExit\r\n";
          snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
          // not disp
          // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
          break;
  case 2: text = "\r\nPlay again\r\n\r\n";
          snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
          // not disp
          // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
          for(int i=0;i<np;i++){
            memset(player[i].name, 0, sizeof(player[i].name));
            player[i].score = 0;
          }
          // startGame();
          initGame();
          break;
    default: text = "\r\nInvalid Input!\r\n";
            snprintf((char *) sendbuf, strlen(text)+1, "%s", text);
            // HAL_UART_Transmit(&huart1, sendbuf, strlen((char *) sendbuf), 10);
            lastLine = displayStringOnLCDAtLine(sendbuf, lastLine+2, 21);
            checkplay();
            break;
  }
}
